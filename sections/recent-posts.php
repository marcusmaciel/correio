<?php
    $new_query = new WP_Query( array(
        'post_per_page' => 5,
        'showposts' => 5,
        'order' => 'DESC',
        'orderby' => 'date'
    ));
    while($new_query->have_posts()) : $new_query->the_post();
       ?>
           <div class="recent-posts-wp">
                <div class="box-recent-posts">
                <a href="<?=get_the_permalink($post->ID)?>">
                <div class="box-image" style='background: url(" <?=get_the_post_thumbnail_url($post->ID, 'full');?> ") center center no-repeat; background-size: cover;"'>
                <span class="contador">
                <?php
                    echo  $new_query->current_post +1;
                ?>
                </span>

            </div>
            </a>
               <div class="descricao">
               <h4><a href="<?=get_the_permalink($post->ID)?>"><?php the_title(); ?></a></h4>
               </div>
                </div>
           </div>
            
        <?php
    endwhile;
    wp_reset_postdata();
?>