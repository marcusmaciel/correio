<section class="social-cards">
    <ul>
    <?php 
        $links = getSocialLinks();
        foreach($links as $key){
            ?>
            <li>
                <a href="<?=$key['url'];?>">
                    <i class="fa <?=$key['icone'];?>"></i>
                    <?=$key['nome'];?>
                    </a>
            </li>
            <?php
        }
    ?>
    </ul>
</section>