
<section class="bottom-posts">
    <div class="container">
        <h4 class="title-section">Outros conteúdos</h4>
        <div class="row">
            
            <?php

    $args = array(
        'post-type' => 'post',
        'status' => 'publish',
    );

    $q = new WP_Query($args);
    if ( $q->have_posts() ) {
        $count = 1;
        while ( $q->have_posts() ) {

            $q->the_post();
            $titulo = get_the_title();
            $imagem = get_the_post_thumbnail_url($q->post->ID, 'medium');

            $author = $q->post->post_author;
            $autor = get_the_author_meta( 'display_name', $author);
            $urlAuthor = get_author_posts_url($author);
            ?>

            <div class="col-md-3 col-xs-12">
                <img src="<?=$imagem;?>" alt="<?=$titulo;?>" title="<?=$titulo;?>">
                
                <strong>
                    <?php
                        $categories = get_the_category($q->post->ID);
                        $cat_link = get_category_link($category[0]->cat_ID);
                        echo '<a href="'.$cat_link.'">'.$categories[0]->cat_name.'</a>'
                    ?>
                </strong>
                <h4>
                    <a href="<?=the_permalink();?>">
                    <?=$titulo;?>
                </a>
                </h4>
                <span><?php echo get_the_time('j \d\e F',$q->post->ID);?> | Por <a href="<?=$urlAuthor;?>"><?=$autor;?></a></span>
            </div>
            <?php
            if(($count % 4) == 0){
                echo '<div class="clear"></div>';
            }
            $count++;
        }

    }

    wp_reset_query();

?>   
        </div>
    </div>
</section>