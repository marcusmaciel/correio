<section class="colunistas">
    <div class="container">
        <h3 class="title-section">Colunistas</h3>
        <div class="row">
            <?php
                $users = get_users();
                $count = 1;
                foreach ($users as $user) 
                {
                    $nome = $user->display_name;
                   $foto = get_avatar_url($user->user_email);
                   $bio = $user->description;
                   $url = get_author_posts_url($user->ID);
                    ?>
                    <div class="col-md-4">
                                <div class="colunista-box" onclick="location.href='<?=$url;?>'">
                                        <div class="img-colunista">
                                            <img src="<?=$foto;?>" alt="<?=$nome;?>" title="<?=$nome;?>" class="img-responsive" />
                                        </div>
                                        <div class="descricao-colunista">
                                            <h4>
                                                <a href="<?=$url;?>" rel="follow" title="<?=$nome?>">
                                                    <?=$nome;?>
                                                </a>
                                            </h4>
                                            <p>
                                                <?=$bio;?>
                                            </p>
                                            <a href="<?=$url;?>" class="btn-colunista">
                                                Ler matérias
                                            </a>
                                        </div>
                                    </div>
                   </div>

            <?php
                   if(($count % 3) == 0){
                       echo '<div class="clear"></div>';
                   }

            $count++;
                }
            ?>
        </div>
    </div>
</section>