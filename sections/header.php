<section class="top-header">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <ul class="bagde-list">
                    <li>
                        <a href="#" class="badge-top">
                            Categoria
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>
                                Prefeitura de Conganhas divulga resultado do processo seletivo
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-5">
                    <?php
                    wp_nav_menu( array(
                        'menu' => 'menu-topo',
                        'class' => 'top-menu'
                    ) );
                    ?>
            </div>
        </div>
    </div>
</section>
<header class="header-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-9 col-xs-9">
                <?php
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                if ( has_custom_logo() ) {
                        echo '
                        <a href="'. get_option("home") .'">    
                        <img src="'. esc_url( $logo[0] ) .'"></a>';
                } else {
                        echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
                }
                ?>
            </div>
            <div class="col-md-9 col-xs-3 col-sm-3">
                <ul class="menu-header">
                    <li>
                        <a href="#">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-whatsapp"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-pinterest"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="button-cta">
                            Quero anunciar aqui
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>