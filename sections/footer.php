<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
            <img
                    src="<?php
                        echo get_template_directory_uri() . '/assets/img/logo-white.png'
                    ?>"
                    alt="<?=bloginfo('name');?> - <?=bloginfo('description');?>"
                    title="<?=bloginfo('name');?> - <?=bloginfo('description');?>"/>
                    <p>
                    O Correio de Minas é conhecido por
ser o jornal mais famoso de Conselheiro Lafaiete, como diz nossa
marca o nosso diferencial está no
conteúdo e na rapidez em disponibilizar noticias para você.

                    </p>
            </div>
            <div class="col-md-3">
                <h3>Onde estamos</h3>
                <?php
                    $endereco = get_field('endereco', 'option');
                    $atendimento = get_field('email', 'option');
                    $telefone = get_field('telefone', 'option');
                    
                ?>
                <?=$endereco;?>
                <p>
                    <span class="email">
                        <a href="mailto:<?=$atendimento;?>"><?=$atendimento;?></a>
                    </span>
                </p>
                <p><span class="tel">
                    <a href="callto:<?=$telefone?>"><?=$telefone;?></a>
                </span></p>
            </div>
            <div class="col-md-3">
                <h3>Novidades</h3>

                <p>Inscreva-se para receber nossas novidades diretamente em seu e-mail.</p>
                <?=do_shortcode('[contact-form-7 id="7" title="news"]');?>

            </div>
            <div class="col-md-3">
                <h3>Nuvem de tags</h3>
            </div>
        </div>
    </div>
</footer>
<section class="copy">
    <div class="container">
        <ul>
            <li>
                <strong>
                    Copyright. Todos os direitos reservados - 2018. Correio de Minas
                </strong>
            </li>
            <li>
                Desenvolvido com carinho por <a href="https://agenciaklik.com.br?utm_source=cliente&utm_term=correiodeminas" target="new" rel="follow">
                    <img src="<?php
                        echo get_template_directory_uri() . '/assets/img/logo-klik.png'
                    ?>" alt="Agência Klik - Criação de sites e Inbound Marketing em BH" title="Agência Klik - Criação de sites e Inbound Marketing em BH">
                </a>
            </li>
        </ul>
    </div>
</section>