<section class="contato">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>Deixe uma mensagem</h1>
                <?php
                    echo do_shortcode('[contact-form-7 id="32" title="contato"]');
                ?>
            </div>
            <div class="col-md-4">
                <?php
                   get_sidebar();
                ?>
            </div>
        </div>
    </div>
</section>