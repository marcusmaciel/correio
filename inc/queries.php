<?php


function addRecent($attr, $content){
    ob_start();  
    get_template_part('sections/recent-posts', 'posts');  
    $ret = ob_get_contents();  
    ob_end_clean();  
    return $ret;   
}

add_shortcode('recent-posts-klik', 'addRecent');

function addSocialCards($attr, $content){
    ob_start();  
    get_template_part('sections/social-cards');  
    $ret = ob_get_contents();  
    ob_end_clean();  
    return $ret;   
}

add_shortcode('social-cards', 'addSocialCards');


function getSocialLinks(){
    static $social;

    if(!$social):
        $list = get_field('links_para_redes_sociais', 'option');
        $links = array();
        foreach($list as $key => $rede){
            $links[$key] = array(
                'icone' => $rede['icone'],
                'nome' => $rede['nome'],
                'url' => $rede['url'],
            );
        }
    endif;
    return $links;
}