<?php

function correio_setup(){
    add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );

	// get the the role object and add $cap capability to this role object
	$role_object = get_role( 'editor' );
	$role_object->add_cap( 'edit_theme_options' );

    add_image_size('servicos-thum', 308, 183, true);
}

add_action('after_setup_theme', 'correio_setup');

function correio_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'correio_excerpt_length' );

function correio_init() {
	// Register menus
	register_nav_menus(array(
        'header-menu' => __( 'Header Menu' ),
        'top-menu' => __( 'Top Menu' ),
		'footer-menu' => __( 'Footer Menu' )
	));
}
add_action( 'init', 'correio_init', 10, 0 );


function correio_custom_logo_setup() {
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'correio_custom_logo_setup' );

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page( array(
      'page_title'  => 'Correio de Minas',
      'menu_title'  => 'Correio de Minas',
    ) );
}

function odin_admin_remove_dashboard_widgets() {
	// remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	// remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	// Yoast's SEO Plugin Widget
	remove_meta_box( 'yoast_db_widget', 'dashboard', 'normal' );
}
add_action( 'wp_dashboard_setup', 'odin_admin_remove_dashboard_widgets' );