<?php

function addStyles(){

    $theme_uri = get_template_directory_uri();
    wp_enqueue_style( 'libs', $theme_uri . '/assets/css/libs.css', array(), 1);
    wp_enqueue_style( 'correio-de-minas', $theme_uri . '/assets/css/correio-de-minas.min.css', array(), 1 );

}

add_action('wp_enqueue_scripts', 'addStyles');

function addScripts(){
    $theme_uri = get_template_directory_uri();
    
    wp_enqueue_script( 'owl-carousel', $theme_uri . '/assets/js/owl.carousel.js', array('jquery'), 1.0, true);
    wp_enqueue_script( 'app', $theme_uri . '/assets/js/app.min.js', array('jquery'),1.0, true);
    
}
function addScriptsApp(){
    $theme_uri = get_template_directory_uri();
    wp_enqueue_script( 'app', $theme_uri . '/assets/js/app.min.js', array('jquery'),1.0, true);
    
}
add_action('wp_enqueue_scripts', 'addScripts');
add_action('wp_enqueue_scripts', 'addScriptsApp');
