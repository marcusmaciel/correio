<?php

function correio_menu_page_removing() {
    remove_menu_page( 'edit-comments.php' ); // Remove comments
}
add_action( 'admin_menu', 'correio_menu_page_removing' );

function sidebar(){
    register_sidebar( array(
        'name' => 'Sidebar Right',
        'id' => 'sidebar-right',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ) );

    register_sidebar( array(
        'name' => 'Anúncios do topo',
        'id' => 'top-ads',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ) );
}
add_action( 'widgets_init', 'sidebar' );

if (!function_exists('set_magic_quotes_runtime')) {
    function set_magic_quotes_runtime($new_setting) {
        return true;
    }
}